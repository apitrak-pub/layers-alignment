"""
Layer alignment: align image layers
Copyright (c) 2018 Apitrak SAS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import sys
import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
from tkinter.filedialog import askopenfilenames

class AutoScrollbar(ttk.Scrollbar):
    ''' A scrollbar that hides itself if it's not needed.
        Works only if you use the grid geometry manager '''
    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            self.grid_remove()
        else:
            self.grid()
            ttk.Scrollbar.set(self, lo, hi)

    def pack(self, **kw):
        raise tk.TclError('Cannot use pack with this widget')

    def place(self, **kw):
        raise tk.TclError('Cannot use place with this widget')

class Window(ttk.Frame):
  def __init__(self, mainframe, path):
    ''' Initialize the main Frame '''
    ### Main frame
    
    ttk.Frame.__init__(self, master=mainframe)
    self.master.title('Layer alignement')
   
    self.im  = Image.open(path)
    tkimage = ImageTk.PhotoImage(self.im)
    
    ### Local variables:
    
        # Zoom local variables:
    self.zoom_unit = 1.2
    self.imscale = 1.0  # scale for the canvaas image  
        # List of floors:
    self.floors = []
    
    ### Image Canvas:
    
    self.ImageCanvas = tk.Canvas(self.master,width =self.im.width, height=self.im.height, 
                              background = 'black',highlightthickness=0,
                              scrollregion=[0,0,self.im.width,self.im.height]) 
    self.ImageCanvas.grid(row=0,column=0,rowspan=7,columnspan=7,sticky=tk.NSEW)
    
    ### Make widgets inside frame expandable:
    
    i = 0                                              
    for i in range(10)  :
      self.master.rowconfigure(i, weight=1)
      self.master.columnconfigure(i, weight=1)    
        
    ### Scrollbars:
    
        # Scrollbarx:
    self.scrollx = AutoScrollbar(self.master, orient = 'horizontal', command=self.scroll_x )
    self.ImageCanvas.configure(xscrollcommand=self.scrollx.set)
    self.scrollx.grid(row=7,column=0,columnspan=7,sticky='we')    
        # Scrollbary :      
    self.scrolly = AutoScrollbar(self.master, orient = 'vertical'  , command=self.scroll_y )
    self.ImageCanvas.configure(yscrollcommand=self.scrolly.set)
    self.scrolly.grid(row=0,column=7,rowspan=7,sticky='ns')
    
    ### Buttons:
    
        # Run button:
    self.browse_button = tk.Button(self.master, borderwidth=4, width=10 ,relief=tk.RAISED,
    text='Run',background='lawngreen',comman=self.run).grid(row=0,column=8,columnspan=2 )    
        # Browse button: 
    self.browse_button = tk.Button(self.master, borderwidth=4, width=10 ,relief=tk.RAISED,
    text='Browse',comman=self.browse).grid(row=1,column=8,columnspan=2 )
        # Undo button :
    self.undo_button  = tk.Button(self.master, borderwidth=4 , width=10,relief=tk.RAISED , 
     text='Undo',command=self.undo).grid(column=8, row=8)        
        # Apply button:
    self.apply_button = tk.Button(self.master, borderwidth=4 , width=10,relief=tk.RAISED ,
    text='Apply',command=self.apply).grid(column=9, row=9)
        # Reset button:
    self.reset_button = tk.Button(self.master, borderwidth=4 , width=10,relief=tk.RAISED ,
     text='Reset',command=self.reset).grid(column=8, row=9)
        # Set button  :
    self.set_button   = tk.Button(self.master, borderwidth=4 , width=10,relief=tk.RAISED ,
    text='Set',command=self.set).grid(column=9, row=8)
    
    ### Labels:
    
        # Selected pixels Label:       
    self.Pixels = tk.StringVar()
    self.display_pixels_label = tk.Label(self.master,relief=tk.SOLID,borderwidth=1, 
    textvariable = self.Pixels,width=16).grid(column=8, row=7,columnspan=2)    
        # Images:
    self.images_label = tk.Label(self.master ,width=16,text ='Layers',bg='gray70').grid(column=8
    ,row=2,columnspan=2)    
        # Coordinates:
    self.coordinates_label = tk.Label(self.master,width=16,text='Coordinates',bg='gray70').grid(column=8
    ,row=6,columnspan=2,
    sticky='S')
        # Coordinate A:
    self.A_label = tk.Label(self.master,text='A').grid(column=0,row=8,columnspan=2)
        # Coordinate B:
    self.B_label = tk.Label(self.master,text='B').grid(column=2,row=8,columnspan=2)      
        # Coordinate C:
    self.C_label = tk.Label(self.master,text='C').grid(column=4,row=8,columnspan=2)
     
    self.ImageCanvas.update() 
    # Wait for canvas to create
    # (0,0) and anchor , if anchor = center , center of image will be put to (0.0), hence half of the image  won't apear
    #self.ImageCanvas.create_image(0 ,0 ,image = tkimage,anchor ='nw')  
    self.ImageCanvas.image = tkimage
    self.container = self.ImageCanvas.create_rectangle(0, 0, self.im.width, self.im.height, width=0, outlineoffset='3,3') 
       
    
    ### Events bindings:
    self.ImageCanvas.bind('<Configure>', self._update_image)  # Window size changed.
    self.ImageCanvas.bind('<MouseWheel>',  self.wheel)  # with Windows and MacOS
    self.ImageCanvas.bind('<Button-5>',   self.wheel)  # only with Linux, wheel scroll down
    self.ImageCanvas.bind('<Button-4>',   self.wheel)  # only with Linux, wheel scroll up   
    self.ImageCanvas.bind('<ButtonPress-1>', self.move_from)
    self.ImageCanvas.bind('<B1-Motion>',     self.move_to)
    self.ImageCanvas.bind('<ButtonPress-3>', self.get_coordinates) 
    self._update_image() 
  def run(self):
    print('Got it : Running')
     
  def browse(self):
    filenames = askopenfilenames(title="Open an image",filetypes=[('png files','.png'),('all files','.*')])
    self.__formatpaths(filenames)
    
    
  def undo(self):
    pass
  def apply(self):
    pass
  def reset(self):
    pass
  def set(self):
    pass
  
  def __formatpaths(self,filenames):
    ''' Method to retrieve only file name without its the full path '''
    self.floors = []
    for filename in filenames:

      filename = filename[filename.rfind('/')+1:]
      self.floors.append(filename)
    ### Radiobuttons:      
       # For each floor:
    i=0
    var = tk.IntVar()
    for floor in self.floors:
      tk.Radiobutton(self.master, text=floor,value=i+1,width=16, variable=var).grid(row=i+3,
      column=8,columnspan=2)
      i=i+1
    var.set(1)
      
  def scroll_y(self, *args, **kwargs):
      ''' Scroll canvas vertically and redraw the image '''
      self.ImageCanvas.yview(*args, **kwargs)  # scroll vertically
      self._update_image()  # redraw the image

  def scroll_x(self, *args, **kwargs):
      ''' Scroll canvas horizontally and redraw the image '''
      self.ImageCanvas.xview(*args, **kwargs)  # scroll horizontally
      self._update_image()  # redraw the image
      
  def move_from(self, event):
      ''' Remember previous coordinates for scrolling with the mouse '''
      self.ImageCanvas.scan_mark(event.x,event.y)
      #print('Marked'+'('+str(event.x)+','+str(event.y)+')')
      
  def canvas_coordinates(self,event):
    x = self.ImageCanvas.canvasx(event.x) #Coordinate of window pixel on the canvas
    y = self.ImageCanvas.canvasy(event.y)
    return x,y    
  
  def coordinates_inside(self,event):
    ''' Test if coordinates are inside image Canvas return True or Flase '''
    x,y = self.canvas_coordinates(event)
    bbox = self.ImageCanvas.bbox(self.container)  # get image area
    bbox = (0,0,bbox[2]-bbox[0],bbox[3]-bbox[1])
    if (bbox[0] < x < bbox[2]) and (bbox[1] < y < bbox[3]): return True
    else: return False
          
  def get_coordinates(self,event):
    ''' Returns correct coordinates proper to Image '''
    if self.coordinates_inside(event) : pass
    else : return   
    import math
    # Position reel = ((Position sur image)+Offset)/Echelle
    self.Pixels.set('('+
         str(math.floor((self.ImageCanvas.canvasx(0)+event.x)/self.imscale))+
         ','+ 
         str(math.floor((self.ImageCanvas.canvasy(0)+event.y)/self.imscale))+
         ')')
     
  def move_to(self, event):
    ''' Drag (move) canvas to the new position '''
    bbox   = self.calculate_regions(0)
    #sys.stdout.write('\rBBOX area {} {} {} {} \n'.format(*bbox))
    bbox1  = self.calculate_regions(1)
    bbox2 = (self.ImageCanvas.canvasx(0),  # get visible area of the canvas
             self.ImageCanvas.canvasy(0),
             self.ImageCanvas.canvasx(self.ImageCanvas.winfo_width()), # Pixel value of the window width on the canvas
             self.ImageCanvas.canvasy(self.ImageCanvas.winfo_height()))
    condition1 = (bbox[0] == bbox2[0] and bbox[2] == bbox2[2])
    condition2 = (bbox[1] == bbox2[1] and bbox[3] == bbox2[3])
    if (bbox1[2] < bbox2[2]) or (bbox1[3] < bbox2[3]): return
    if not (condition1 and condition2) : # Conditions to prevent draging image when it's fully diplayed
      #print('Moving to')    
      self.ImageCanvas.scan_dragto(event.x, event.y, gain=1)
      self._update_image()      
                 
  def wheel(self, event):
    ''' Zoom with mouse wheel '''   
    if self.coordinates_inside(event) : pass
    else : return
    x,y = self.canvas_coordinates(event)
    scale = 1.0
    if event.num == 5 or event.delta == -120:  # scroll down
      i = min(self.im.width, self.im.height)
      if int(i * self.imscale) < 500: return  # image is less than 30 pixels
      self.imscale /= self.zoom_unit
      scale        /= self.zoom_unit
    if event.num == 4 or event.delta == 120:  # scroll up
      j = min(self.ImageCanvas.winfo_width(), self.ImageCanvas.winfo_height())
      if j < self.imscale: return  # 
      self.imscale *= self.zoom_unit
      scale        *= self.zoom_unit
    #print('Image scale: '+ str(self.imscale) )
    self.ImageCanvas.scale('all', x, y, scale, scale)  # rescale all canvas objects
    self._update_image()       

  def calculate_regions(self,i):
    Res = []
    bbox1 = self.ImageCanvas.bbox(self.container)   # get image area meaning Image size
    bbox1 = (0,0,bbox1[2]-bbox1[0],bbox1[3]-bbox1[1])
    bbox2 = (0,  # get visible area of the canvas
             0,
             self.ImageCanvas.canvasx(self.ImageCanvas.winfo_width()), # Pixel value of the window width on the canvas
             self.ImageCanvas.canvasy(self.ImageCanvas.winfo_height()))
    bbox = [min(bbox1[0], bbox2[0]), min(bbox1[1], bbox2[1]),  # get scroll region box
            max(bbox1[2], bbox2[2]), max(bbox1[3], bbox2[3])]
    Res = [bbox,bbox1,bbox2]
    return Res[i]
    
  def _update_image(self,event = None):
    ''' each time an event (Moouse wheel zoom, image scroll...) occurs the image should be updated on canvas'''
    bbox   = self.calculate_regions(0)
    bbox1  = self.calculate_regions(1)
    #sys.stdout.write('\rBBOX area {} {} {} {} \n'.format(*bbox))
    bbox2  = self.calculate_regions(2)        
    if bbox[0] == bbox2[0] and bbox[2] == bbox2[2]:  # whole image in the visible area
      bbox[0] = bbox1[0]
      bbox[2] = bbox1[2]
    if bbox[1] == bbox2[1] and bbox[3] == bbox2[3]:  # whole image in the visible area
      bbox[1] = bbox1[1]
      bbox[3] = bbox1[3]
    self.ImageCanvas.configure(scrollregion=bbox)  # set scroll region
    x1 = max(bbox2[0], 0)  # get coordinates (x1,y1,x2,y2) of the image tile
    y1 = max(bbox2[1], 0)
    x2 = min(bbox2[2], bbox1[2])
    y2 = min(bbox2[3], bbox1[3])
        
    if int(x2 - x1) > 0 and int(y2 - y1) > 0:  # show image if it in the visible area
      _x2 = min(int(x2 / self.imscale), self.im.width)   # sometimes it is larger on 1 pixel...
      _y2 = min(int(y2 / self.imscale), self.im.height)  # ...and sometimes not
      _x1 = int(x1 / self.imscale)
      _y1 = int(y1 / self.imscale)
      image = self.im.crop((_x1, _y1, _x2, _y2))
    img_resize_xy = (int(x2 - x1), int(y2 - y1))
    #########################################################
    imagetk = ImageTk.PhotoImage(image.resize(img_resize_xy))
    #########################################################
    img_position = (max(bbox2[0], bbox1[0]), max(bbox2[1], bbox1[1]))
    imageid = self.ImageCanvas.create_image(img_position, anchor='nw',image=imagetk)
            
    self.ImageCanvas.lower(imageid)  # set image into background
    self.ImageCanvas.imagetk = imagetk  # keep an extra reference to prevent garbage-collection
