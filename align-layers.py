#!/usr/bin/env python3.5
"""
Layer alignment: align image layers
Copyright (c) 2018 Apitrak SAS

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import cv2
import numpy as np

layer_reference = "layer_ref.png"
layer_dest = "layer-corrected.png"

From  = cv2.imread(layer_reference)

rows,cols,ch = From.shape

## From Coords 

A           = [4626,561]
B           = [2223,980]
C           = [2746,1699]

## To Coords

Aprime      = [4770,622 ]
Bprime      = [2400,1047]
Cprime      = [2912,1748]

ptsabc_prime = np.float32([Aprime,Bprime,Cprime])
ptsabc       = np.float32([A,B,C])

Mabc   = cv2.getAffineTransform(ptsabc,ptsabc_prime)
dstabc = cv2.warpAffine(From,Mabc,(cols,rows),borderMode = 1,borderValue = 255)
cv2.imwrite(layer_dest,dstabc)
